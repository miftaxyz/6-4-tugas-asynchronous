var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

const read = async(time,books,i) => { 
	 if(i<books.length){
		await readBooksPromise(time, books[i])
		if(time>0){
				time-=books[i].timeSpent
				i+=1;
				read(time, books,i);
			}
     }
}

read(10000, books,0)